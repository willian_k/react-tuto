const build = require('buildscript-utils')


const tasks = {}

tasks.init = function () {
    return build.spawn('./node_modules/.bin/jspm install')
}

tasks.watch = function () {
    build.spawn('./node_modules/.bin/tsc -w')
}

tasks.serve = function (silent) {
    build.spawn('./node_modules/.bin/http-server', './public', null, silent)
}

build.runTask(tasks)