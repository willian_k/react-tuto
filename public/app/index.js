"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ReactDOM = require("react-dom");
const React = require("react");
const table_1 = require("./table");
//keynes
const Root = class extends React.Component {
    constructor() {
        super(...arguments);
        this.state = { data: data, newName: '' };
    }
    doBind(path) {
        return {
            name: path,
            value: this.state[path],
            onChange: (ev) => {
                let val = ev.target ? ev.target.value : ev;
                this.setState({ [path]: val });
            }
        };
    }
    tableBind(path) {
        let binding = this.doBind(path);
        binding.value = binding.value.map(line => {
            line[1] = String(line[1]).toUpperCase();
            return line;
        });
        return binding;
    }
    render() {
        return React.createElement("div", null,
            React.createElement(table_1.Table, Object.assign({ cols: ['ID', 'Name'] }, this.tableBind('data'), { editable: true })));
    }
};
let data = [[1, 'João'], [2, 'Maria']];
ReactDOM.render(React.createElement(Root, null), document.getElementById('container'));
