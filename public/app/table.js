"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const React = require("react");
const initialTableState = { newValue: '' };
class Table extends React.Component {
    constructor() {
        super(...arguments);
        this.state = initialTableState;
    }
    buttonClick(ev) {
        let toSend = [
            ...this.props.value,
            [this.props.value.length + 1, this.state.newValue]
        ];
        this.props.onChange(toSend);
    }
    render() {
        return React.createElement("div", null,
            this.props.editable && React.createElement("div", null,
                React.createElement("input", { type: "text", value: this.state.newValue, onChange: ev => this.setState({ newValue: ev.target.value }) }),
                React.createElement("button", { onClick: ev => this.buttonClick(ev) }, "Criar novo")),
            React.createElement("table", null,
                React.createElement("thead", null,
                    React.createElement("tr", null, this.props.cols.map((col, idx) => React.createElement("th", { key: idx }, col)))),
                React.createElement("tbody", null, this.props.value.map((line, lineidx) => React.createElement("tr", { key: lineidx }, line.map((col, colidx) => React.createElement("td", { key: colidx }, col)))))));
    }
}
exports.Table = Table;
