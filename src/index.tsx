import ReactDOM = require('react-dom')
import React = require('react')

import { Table } from './table'

type PropsT = {}
type StateT = { data: any[][], newName: string }
//keynes
const Root = class extends React.Component<PropsT, StateT> {

    state = { data: data, newName: '' }

    doBind(path) {
        return {
            name: path,
            value: this.state[path],
            onChange : (ev) => {
                let val = ev.target ? ev.target.value : ev
                this.setState({ [path]: val })
            }
        }
    }


    tableBind(path) {
        let binding = this.doBind(path)
        binding.value = binding.value.map(line => {
            line[1] = String(line[1]).toUpperCase()
            return line
        })
        return binding
    }

    render() {
        return <div>
            <Table
                cols={['ID', 'Name']}
                {...this.tableBind('data') }
                editable
            />
        </div>
    }
}


let data = [[1, 'João'], [2, 'Maria']]
ReactDOM.render(<Root/>, document.getElementById('container'))