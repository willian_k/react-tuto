import React = require('react')

const initialTableState = { newValue : '' }
type State = typeof initialTableState
type Props = {
    value: any[][]
    cols: string[]
    onChange: (val: any[][]) => any
    editable?: boolean
}
export class Table extends React.Component<Props, State> {

    state = initialTableState


    buttonClick(ev) {
        let toSend =  [
            ... this.props.value,
            [this.props.value.length + 1, this.state.newValue]
        ]
        this.props.onChange(toSend)
    }


    render() {
        return <div>
            {this.props.editable && <div>
                <input type="text"
                    value={this.state.newValue}
                    onChange={ev => this.setState({ newValue: ev.target.value })}
                />
                <button onClick={ev => this.buttonClick(ev)} >
                    Criar novo
                </button>
            </div>}

            <table>
                <thead>
                    <tr>
                        {this.props.cols.map((col, idx) => <th key={idx}>{col}</th>)}
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.value.map((line, lineidx) =>
                            <tr key={lineidx}>
                                {line.map((col, colidx) =>
                                    <td key={colidx}>{col}</td>
                                )}
                            </tr>
                        )
                    }
                </tbody>
            </table>
        </div>
    }

}